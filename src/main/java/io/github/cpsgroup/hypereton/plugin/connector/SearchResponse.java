package io.github.cpsgroup.hypereton.plugin.connector;

import java.util.List;

/**
 * Created by Manuel Weidmann on 14.09.2014.
 */
public class SearchResponse {
    private int numFound;
    private int numReturned;
    private List<String> facetList;
    private String[][] lawArray;

    public int getNumFound() {
        return numFound;
    }

    public void setNumFound(int numFound) {
        this.numFound = numFound;
    }

    public int getNumReturned() {
        return numReturned;
    }

    public void setNumReturned(int numReturned) {
        this.numReturned = numReturned;
    }

    public String[][] getLawArray() {
        return lawArray;
    }

    public void setLawArray(String[][] lawArray) {
        this.lawArray = lawArray;
    }
}
