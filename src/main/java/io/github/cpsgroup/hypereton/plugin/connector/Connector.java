package io.github.cpsgroup.hypereton.plugin.connector;


import io.github.cpsgroup.hypereton.plugin.Plugin;

/**
 * Interface for hypereton plugins.
 * Implementing classes must be annotated with org.springframework.stereotype.Component in order for plugin loading to work.
 * <p/>
 * Created by Manuel Weidmann on 06.09.2014.
 */
public interface Connector extends Plugin {

    /**
     * Checks whether the server is reachable or not
     *
     * @return boolean
     */
    public boolean ping();


    /**
     * Search by term
     *
     * @param searchTerm the term to search for
     * @return list of results
     */
    public SearchResponse searchByString(String searchTerm);


}
